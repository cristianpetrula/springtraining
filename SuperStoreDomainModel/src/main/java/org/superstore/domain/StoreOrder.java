/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.superstore.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author cristian.petrula
 */
@Entity
@Data
@Table(name = "store_order")
@NamedQueries({
        @NamedQuery(name = "StoreOrder.findAll", query = "SELECT s FROM StoreOrder s")})
public class StoreOrder implements Serializable, SuperStoreMessage {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @Column(name = "cost", nullable = false)
    private float cost;
    @JoinColumn(name = "customerId", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Customer customerId;
    @Basic(optional = false)
    @Column(name = "destination_address", nullable = false, length = 255)
    private String destinationAddress;
    @Basic(optional = false)
    @Column(name = "destination_name", nullable = false, length = 255)
    private String destinationName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "storeOrderId")
    private List<ShipmentStatus> shipmentStatusList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "storeOrderId")
    private List<StoreOrderItem> storeOrderItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "storeOrderId")
    private List<Payment> paymentList;

    public StoreOrder() {
    }

    public StoreOrder(Integer id) {
        this.id = id;
    }

    public StoreOrder(Integer id, Date date, float cost) {
        this.id = id;
        this.date = date;
        this.cost = cost;
    }
}
