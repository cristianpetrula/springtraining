/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.superstore.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author cristian.petrula
 */
@Entity
@Data
@Table(name = "shipment_status")
@NamedQueries({
        @NamedQuery(name = "ShipmentStatus.findAll", query = "SELECT s FROM ShipmentStatus s")})
public class ShipmentStatus implements Serializable, SuperStoreMessage {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "comment", length = 255)
    private String comment;
    @Basic(optional = false)
    @Column(name = "date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 255)
    private String status;
    @Basic(optional = false)
    @Column(name = "storeOrderId", nullable = false)
    private Integer storeOrderId;

    public ShipmentStatus() {
    }

    public ShipmentStatus(Integer id) {
        this.id = id;
    }

    public ShipmentStatus(Integer id, Date date, String status) {
        this.id = id;
        this.date = date;
        this.status = status;
    }
}
