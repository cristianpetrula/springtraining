/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.superstore.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author cristian.petrula
 */
@Entity
@Data
@Table(name = "item")
@NamedQueries({
        @NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i")})
public class Item implements Serializable, SuperStoreMessage {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 255)
    private String code;
    @Basic(optional = false)
    @Column(name = "description", nullable = false, length = 300)
    private String description;
    @Basic(optional = false)
    @Column(name = "image", nullable = false, length = 100)
    private String image;
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 255)
    private String name;
    @Basic(optional = false)
    @Column(name = "stock", nullable = false)
    private Integer stock;
    @Basic(optional = false)
    @Column(name = "price", nullable = false)
    private Double price;

    public Item() {
    }

    public Item(Integer id) {
        this.id = id;
    }

    public Item(Integer id, String code, Double price) {
        this.id = id;
        this.code = code;
        this.price = price;
    }
}
