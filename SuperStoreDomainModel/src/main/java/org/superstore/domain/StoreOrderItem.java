/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.superstore.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author cristian.petrula
 */
@Entity
@Data
@Table(name = "store_order_item")
@NamedQueries({
        @NamedQuery(name = "StoreOrderItem.findAll", query = "SELECT s FROM StoreOrderItem s")})
public class StoreOrderItem implements Serializable, SuperStoreMessage {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "count", nullable = false)
    private int count;
    @JoinColumn(name = "itemId", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Item itemId;
    @Basic(optional = false)
    @Column(name = "storeOrderId", nullable = false)
    private Integer storeOrderId;

    public StoreOrderItem() {
    }

    public StoreOrderItem(Integer id) {
        this.id = id;
    }

    public StoreOrderItem(Integer id, int count) {
        this.id = id;
        this.count = count;
    }
}
