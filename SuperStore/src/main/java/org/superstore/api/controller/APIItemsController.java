package org.superstore.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.superstore.api.service.APIItemsService;
import org.superstore.domain.Item;
import org.superstore.dto.ItemListDTO;

import javax.transaction.Transactional;

/**
 * Created by cristian.petrula on 6/9/2015.
 */
@Controller
@RequestMapping("/api/item")
public class APIItemsController {
    @Autowired
    private APIItemsService itemsService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    void save(@RequestBody Item item) {
        itemsService.save(item);
    }

    @RequestMapping(value = "/addlist", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    @Transactional
    void saveLisy(@RequestBody ItemListDTO list) {
        for (Item item : list.getList()) {
            itemsService.save(item);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    ItemListDTO findAll(Pageable pageable) {
        return itemsService.findAll(pageable);
    }
}
