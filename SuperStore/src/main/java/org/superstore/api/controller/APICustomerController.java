package org.superstore.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.superstore.api.service.APICustomerService;
import org.superstore.domain.Customer;

import java.util.List;

/**
 * Created by cristian.petrula on 7/2/2015.
 */
@Controller
@RequestMapping("/api/customer")
public class APICustomerController {
    @Autowired
    APICustomerService customerService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    List<Customer> findAll() {
        return customerService.findAll();
    }
}
