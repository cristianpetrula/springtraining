package org.superstore.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.superstore.domain.Item;
import org.superstore.dto.ItemListDTO;
import org.superstore.repository.ItemRepository;

/**
 * Created by cristian.petrula on 6/28/2015.
 */
@Service
public class APIItemServiceImpl implements APIItemsService {
    @Autowired
    private ItemRepository itemRepository;

    public ItemListDTO findAll(Pageable pageable) {
        Page<Item> itemsDB = itemRepository.findAll(pageable);
        ItemListDTO result = new ItemListDTO();
        result.setList(itemsDB.getContent());
        return result;
    }

    public void save(Item item) {
        itemRepository.save(item);
    }
}
