package org.superstore.api.service;

import org.superstore.domain.Customer;

import java.util.List;

/**
 * Created by cristian.petrula on 7/2/2015.
 */
public interface APICustomerService {
    public List<Customer> findAll();
}
