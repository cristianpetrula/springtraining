package org.superstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperStoreWithPersistence {
    public static void main(String[] args) {
        SpringApplication.run(SuperStoreWithPersistence.class);
    }
}
