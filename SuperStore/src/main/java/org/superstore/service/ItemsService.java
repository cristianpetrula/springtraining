package org.superstore.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.superstore.domain.Item;
import org.superstore.dto.ItemListDTO;

/**
 * Created by cristian.petrula on 6/8/2015.
 */
public interface ItemsService {
    Page<Item> findAll(Pageable pageable);
    void save(Item item);
}
