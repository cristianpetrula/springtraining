package org.superstore.service;

import org.superstore.domain.Customer;

import java.util.List;

/**
 * Created by cristian.petrula on 7/2/2015.
 */
public interface CustomerService {
    public List<Customer> findAll();
}
