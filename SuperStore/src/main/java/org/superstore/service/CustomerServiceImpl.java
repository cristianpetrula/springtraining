package org.superstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.superstore.domain.Customer;
import org.superstore.repository.CustomerRepository;

import java.util.List;

/**
 * Created by cristian.petrula on 7/2/2015.
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    public List<Customer> findAll() {
        return (List<Customer>) customerRepository.findAll();
    }
}
