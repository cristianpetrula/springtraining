package org.superstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.superstore.domain.Item;
import org.superstore.repository.ItemRepository;

/**
 * Created by cristian.petrula on 6/4/2015.
 */
@Service
public class ItemsServiceImpl implements ItemsService {
    @Autowired
    private ItemRepository itemRepository;

    public Page<Item> findAll(Pageable pageable) {
        Page<Item> itemsDB = itemRepository.findAll(pageable);
        return itemsDB;
    }

    public void save(Item item) {
        itemRepository.save(item);
    }
}
