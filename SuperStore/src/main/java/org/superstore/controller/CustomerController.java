package org.superstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.superstore.service.CustomerService;

/**
 * Created by cristian.petrula on 7/2/2015.
 */
@Controller
@RequestMapping("/customers")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @RequestMapping("")
    public String findAll(Model model) {
        model.addAttribute("customers", customerService.findAll());
        return "customer";
    }
}
