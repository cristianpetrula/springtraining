package org.superstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.superstore.service.ItemsServiceImpl;

/**
 * Created by cristian.petrula on 6/2/2015.
 */
@Controller
@RequestMapping("/")
public class IndexController {
    @Autowired
    private ItemsServiceImpl itemsService;

    @RequestMapping("")
    public String index() {
        return "index";
    }
}
