package org.superstore.page;

import org.springframework.data.domain.Page;
import org.superstore.domain.Item;

/**
 * Created by cristian.petrula on 6/19/2015.
 */
public class ItemsPageWrapper {

    public static final int ITEMS_ON_ROW = 3;
    public static final int ITEMS_ON_PAGE = 21;
    private Page<Item> page;

    public ItemsPageWrapper(Page<Item> page) {
        this.page = page;
    }

    public Page<Item> getPage() {
        return page;
    }

    public int getSize() {
        return ITEMS_ON_PAGE;
    }

    public boolean hasItemAt(int row, int col) {
        boolean result = (page.getNumberOfElements() > calculateIndex(row, col)) ? true : false;
        //System.out.println("Checking " + row + " " + col + " " + calculateIndex(row, col) + " " + result);
        return result;
    }

    public Item getItemAt(int row, int col) {
        return page.getContent().get(calculateIndex(row, col));
    }

    public int getNumber() {
        return page.getNumber();
    }

    public int getTotalPages() {
        return page.getTotalPages();
    }

    public boolean isFirst() {
        return page.isFirst();
    }

    public boolean isLast() {
        return page.isLast();
    }

    public int getRowCount() {
        return new Double(Math.ceil(getSize() / ITEMS_ON_ROW)).intValue();
    }

    private int calculateIndex(int row, int col) {
        //System.out.println(row * ITEMS_ON_ROW + col);
        return row * ITEMS_ON_ROW + col;
    }

    public String getImagePath(int row, int col) {
        return "/media/" + page.getContent().get(calculateIndex(row, col)).getImage();
    }
}
