package org.superstore.page;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created by cristian.petrula on 6/20/2015.
 */
public class CustomPageRequest extends PageRequest {
    public CustomPageRequest(int page, int size) {
        super(page, ItemsPageWrapper.ITEMS_ON_PAGE);
    }

    public CustomPageRequest(int page, int size, Sort.Direction direction, String... properties) {
        super(page, ItemsPageWrapper.ITEMS_ON_PAGE, direction, properties);
    }

    public CustomPageRequest(int page, int size, Sort sort) {
        super(page, ItemsPageWrapper.ITEMS_ON_PAGE, sort);
    }

    public CustomPageRequest(Pageable pageable) {
        super( pageable.getPageNumber(), ItemsPageWrapper.ITEMS_ON_PAGE, pageable.getSort());
    }
}
