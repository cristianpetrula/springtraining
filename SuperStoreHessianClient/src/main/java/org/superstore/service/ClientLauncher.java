package org.superstore.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.superstore.domain.Customer;
import org.superstore.domain.Item;
import util.CustomerGenerator;
import util.params.CustomerGeneratorParams;

import java.util.List;

/**
 * Created by cristian.petrula on 6/8/2015.
 */
public class ClientLauncher {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/client-config.xml");
        //getItemsInStore(context);
        insertCustomers(context);
    }

    private static void getItemsInStore(ApplicationContext context) {
        ItemsService itemService = (ItemsService) context.getBean("itemService");
        List<Item> items = itemService.findAll();
        for (Item item : items) {
            System.out.println(item);
        }
        System.out.println(items.size());
    }

    private static void insertCustomers(ApplicationContext context) {
        CustomerService customerService = (CustomerService) context.getBean("customerService");
        CustomerGeneratorParams param = new CustomerGeneratorParams();
        param.setName("Ion");
        param.setEmail("ion@gmail.com");
        param.setCount(2000);
        CustomerGenerator customerGenerator = new CustomerGenerator();
        List<Customer> customerList = customerGenerator.generateCustomers(param);
        for (Customer customer : customerList) {
            customerService.save(customer);
        }

        System.out.println(customerService.findAll().size());
    }
}
