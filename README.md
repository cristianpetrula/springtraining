# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* SuperStore
* 1.0

### How do I get set up? ###

* Summary of set up
Get the code and run the main class. It has built in servers and all necessary dependencies.
* Configuration
application.properties contains all needed settings
* Dependencies
Spring, Spring Boot
* Database configuration
application.properties again will hold all necessary settings
* How to run tests
We don't have tests yet :(
* Deployment instructions
The applicaiton is packed as a jar.

### Contribution guidelines ###

* Cristian Petrula

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact