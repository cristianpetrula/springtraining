package org.superstore.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.superstore.domain.Customer;
import org.superstore.domain.Item;
import org.superstore.domain.StoreOrder;
import org.superstore.domain.StoreOrderItem;
import org.superstore.repository.CustomerRepository;
import org.superstore.repository.ItemRepository;
import org.superstore.repository.StoreOrderRepository;
import util.SuperStoreRandomUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by cristian.petrula on 7/3/2015.
 */
@Component
public class StoreOrderScheduler {
    @Autowired
    ItemRepository itemRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    StoreOrderRepository storeOrderRepository;

    List<Item> allItems;
    List<Customer> allCustomers;

    @Scheduled(fixedRate = 5000)
    public void insertOrders() {
        List<StoreOrder> orders = generateorders();
        saveOrders(orders);
        System.out.println("You have new " + orders.size() + " orders");
    }

    @Transactional
    private void saveOrders(List<StoreOrder> orders) {
        for (StoreOrder order : orders) {
            storeOrderRepository.save(order);
            order.setStoreOrderItemList(generateStoreOrderItems(order));
            //add alos items
            storeOrderRepository.save(order);
        }
    }

    private List<StoreOrder> generateorders() {
        allItems = (List<Item>) itemRepository.findAll();
        allCustomers = (List<Customer>) customerRepository.findAll();
        List<StoreOrder> orders = new ArrayList<StoreOrder>();
        int maxCount = 10;
        for (int i = 0; i < maxCount; i++) {
            StoreOrder order = new StoreOrder();
            order.setCost((float) SuperStoreRandomUtils.randDouble(10, 10000));
            order.setCustomerId(allCustomers.get(SuperStoreRandomUtils.randInt(0, allCustomers.size() - 1)));
            order.setDate(new Date());
            order.setDestinationAddress("Some address " + i);
            order.setDestinationName("Name " + i);
            orders.add(order);
        }
        return orders;
    }

    private List<StoreOrderItem> generateStoreOrderItems(StoreOrder order) {
        List<StoreOrderItem> items = new ArrayList<StoreOrderItem>();
        int itemCount = SuperStoreRandomUtils.randInt(1, 10);
        for (int i = 0; i < itemCount; i++) {
            StoreOrderItem item = new StoreOrderItem();
            item.setCount(SuperStoreRandomUtils.randInt(1, 20));
            item.setItemId(allItems.get(SuperStoreRandomUtils.randInt(0, allItems.size() - 1)));
            item.setStoreOrderId(order.getId());
            items.add(item);
        }
        return items;
    }
}
