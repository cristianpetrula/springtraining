package org.superstore.scheduler;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by cristian.petrula on 6/28/2015.
 */
@Component
public class importItemsBatchScheduler {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("importItemJob")
    private Job job;

    //@Scheduled(fixedDelay = 10000)
    public void run() {
        try {
            String dateParam = new Date().toString();
            JobParameters param = new JobParametersBuilder().addString("date", dateParam).toJobParameters();

            System.out.println(dateParam);

            JobExecution execution = jobLauncher.run(job, param);
            System.out.println("Exit Status : " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
