package org.superstore.scheduler;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.superstore.batch.OrderValidator;

import java.util.Date;

/**
 * Created by cristian.petrula on 7/3/2015.
 */
@Component
public class MoveOrderScheduler {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("moveOrdersToShipmentJob")
    private Job job;

    @Scheduled(fixedDelay = 20000)
    public void run() {
        try {
            String dateParam = new Date().toString();
            JobParameters param = new JobParametersBuilder().addDate("date", new Date()).toJobParameters();

            //System.out.println(dateParam);

            JobExecution execution = jobLauncher.run(job, param);
            System.out.println("Exit Status move orders: " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
