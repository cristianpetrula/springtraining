package org.superstore.batch;

import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.superstore.domain.ShipmentStatus;
import org.superstore.domain.StoreOrder;
import org.superstore.repository.ShipmentRepository;

/**
 * Created by cristian.petrula on 7/3/2015.
 */
@Component
public class OrderValidator implements Validator<StoreOrder> {

    @Autowired
    ShipmentRepository shipmentRepository;

    public void validate(StoreOrder value) throws ValidationException {
        ShipmentStatus status = shipmentRepository.getByOrderId(value.getId());
        if (status != null) {
            throw new ValidationException("is shiped!");
        }
    }
}
