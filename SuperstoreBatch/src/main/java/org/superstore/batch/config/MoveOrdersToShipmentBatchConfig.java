package org.superstore.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.superstore.batch.OrderProcessor;
import org.superstore.batch.OrderValidator;
import org.superstore.domain.ShipmentStatus;
import org.superstore.domain.StoreOrder;
import org.superstore.repository.ItemRepository;
import org.superstore.repository.ShipmentRepository;
import org.superstore.repository.StoreOrderRepository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cristian.petrula on 7/3/2015.
 */
@Component
public class MoveOrdersToShipmentBatchConfig {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private StoreOrderRepository storeOrderRepository;

    @Autowired
    private OrderValidator orderValidator;

    @Autowired
    @Qualifier("moveOrderStep")
    private Step moveStep;

    @Bean(name = "moveOrdersToShipmentJob")
    public Job moveOrdersToShipmentJob(JobExecutionListener listener) {
        return jobBuilderFactory.get("moveOrdersToShipmentJob").incrementer(new RunIdIncrementer()).listener(listener).
                start(moveStep).build();
    }

    @Bean(name = "moveOrderStep")
    public Step step1(@Qualifier("orderReader") ItemReader<StoreOrder> reader, @Qualifier("shipmentWriter") ItemWriter<ShipmentStatus> writer,
                      @Qualifier("orderProcessor") ItemProcessor<StoreOrder, ShipmentStatus> processor) {
        return stepBuilderFactory.get("moveOrderStep").<StoreOrder, ShipmentStatus>chunk(10).reader(reader).processor(
                processor).writer(
                writer).build();
    }

    @Bean(name = "orderReader")
    public ItemReader<StoreOrder> orderReader() {
        RepositoryItemReader<StoreOrder> reader = new RepositoryItemReader<StoreOrder>();
        reader.setRepository(storeOrderRepository);
        reader.setMethodName("findAll");
        reader.setPageSize(1000);
        Map<String, Sort.Direction> sort = new HashMap<String, Sort.Direction>();
        sort.put("date", Sort.Direction.DESC);
        reader.setSort(sort);
        return reader;
    }

    @Bean(name = "shipmentWriter")
    public ItemWriter<ShipmentStatus> writer(ItemRepository itemRepository) {
        RepositoryItemWriter<ShipmentStatus> writer = new RepositoryItemWriter<ShipmentStatus>();
        writer.setRepository(shipmentRepository);
        writer.setMethodName("save");
        return writer;
    }

    @Bean(name = "orderProcessor")
    public OrderProcessor orderProcessor() {
        OrderProcessor processor = new OrderProcessor();
        processor.setValidator(orderValidator);
        processor.setFilter(true);
        return processor;
    }
}
