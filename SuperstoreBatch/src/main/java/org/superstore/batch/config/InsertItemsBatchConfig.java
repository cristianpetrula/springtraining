package org.superstore.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.builder.TaskletStepBuilder;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.superstore.batch.ArchiveTasklet;
import org.superstore.batch.ItemFieldSetMapper;
import org.superstore.batch.SuperStoreItemValidator;
import org.superstore.batch.SuperstoreItemProcessor;
import org.superstore.domain.Item;
import org.superstore.dto.ItemDTO;
import org.superstore.repository.ItemRepository;

@Component
public class InsertItemsBatchConfig {
    private static String DATA_FOLDER = "e:\\data\\newitems\\";
    private static String ARCHIVE_FOLDER = "archive\\";
    private static String FILE_NAME = "SuperStoreItems_generated_1.csv";
    @Autowired
    @Qualifier("archiveTasklet")
    private Tasklet tasklet;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    @Qualifier("step1")
    private Step step1;
    @Autowired
    @Qualifier("step2")
    private Step step2;

    @Bean(name = "itemReader")
    public ItemReader<ItemDTO> reader() {
        FlatFileItemReader<ItemDTO> reader = new FlatFileItemReader<ItemDTO>();
        reader.setResource(new FileSystemResource(DATA_FOLDER + FILE_NAME));
        //reader.setStrict( false);
        reader.setLineMapper(new DefaultLineMapper<ItemDTO>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[]{"code", "name", "description", "image", "price", "count"});
            }});
            setFieldSetMapper(new ItemFieldSetMapper());
            /*
            setFieldSetMapper(new BeanWrapperFieldSetMapper<ItemDTO>() {{
                setTargetType(ItemDTO.class);
            }});
            */
        }});
        return reader;
    }

    @Bean(name = "itemProcessor")
    public SuperstoreItemProcessor processor() {
        SuperstoreItemProcessor processor = new SuperstoreItemProcessor();
        processor.setValidator(new SuperStoreItemValidator());
        processor.setFilter(true);
        return processor;
    }

    @Bean(name = "itemWriter")
    public ItemWriter<Item> writer(ItemRepository itemRepository) {
        RepositoryItemWriter<Item> writer = new RepositoryItemWriter<Item>();
        writer.setRepository(itemRepository);
        writer.setMethodName("save");
        return writer;
    }

    @Bean(name = "importItemJob")
    public Job importItemJob(JobExecutionListener listener) {
        return jobBuilderFactory.get("importItemJob").incrementer(new RunIdIncrementer()).listener(listener).
                start(step1).next(step2).build();
    }

    @Bean(name = "step1")
    public Step step1(@Qualifier("itemReader") ItemReader<ItemDTO> reader, @Qualifier("itemWriter") ItemWriter<Item> writer,
                      @Qualifier("itemProcessor") ItemProcessor<ItemDTO, Item> processor) {
        return stepBuilderFactory.get("step1").<ItemDTO, Item>chunk(100).reader(reader).processor(processor).writer(
                writer).build();
    }

    @Bean(name = "step2")
    public Step step2() {
        TaskletStepBuilder builder = new TaskletStepBuilder(stepBuilderFactory.get("step2"));
        return builder.tasklet(tasklet).build();
    }

    @Bean(name = "archiveTasklet")
    public Tasklet tasklet() {
        ArchiveTasklet archiveTasklet = new ArchiveTasklet();
        archiveTasklet.setFolder(DATA_FOLDER);
        archiveTasklet.setArchiveFolder(DATA_FOLDER + ARCHIVE_FOLDER);
        archiveTasklet.setFile(FILE_NAME);
        return archiveTasklet;
    }



}
