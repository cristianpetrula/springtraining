package org.superstore.batch.config;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Created by cristian.petrula on 7/3/2015.
 */
@Configuration
@EnableBatchProcessing
public class SuperStorebatchConfiguration {
    @Autowired
    MoveOrdersToShipmentBatchConfig moveConfig;

    @Autowired
    InsertItemsBatchConfig insertItemsBatchConfig;
}
