package org.superstore.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.superstore.domain.Item;
import org.superstore.repository.ItemRepository;

import java.util.List;

/**
 * Created by cristian.petrula on 6/27/2015.
 */
@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {
    private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);
    @Autowired
    private ItemRepository itemRepository;

    @Override
    public void afterJob(JobExecution jobExecution) {
        if ( jobExecution.getJobInstance().getJobName().equals("importItemJob")){
            if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
                log.info("Items were inserted");
            }
        }else  if ( jobExecution.getJobInstance().getJobName().equals("moveOrdersToShipmentJob")){
            System.out.println( "Orders were moved to shipment");
        }

    }
}
