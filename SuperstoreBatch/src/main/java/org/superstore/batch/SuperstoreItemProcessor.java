package org.superstore.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.superstore.domain.Item;
import org.superstore.dto.ItemDTO;

/**
 * Created by petrulac on 24/06/2015.
 */
public class SuperstoreItemProcessor implements ItemProcessor<ItemDTO, Item> {
    private Validator<ItemDTO> validator;
    private boolean filter = true;

    private static final Logger log = LoggerFactory.getLogger(SuperstoreItemProcessor.class);

    public Item process(ItemDTO item) throws Exception {
        try {
            validator.validate(item);
        }
        catch (ValidationException e) {
            if (filter) {
                return null; // filter the item
            }
            else {
                throw e; // skip the item
            }
        }
        Item transformedItem = new Item();
        transformedItem.setCode(item.getCode());
        transformedItem.setDescription(item.getDescription());
        transformedItem.setImage(item.getImage());
        transformedItem.setName(item.getName());
        transformedItem.setStock(item.getCount());
        transformedItem.setPrice(item.getPrice());
        //log.info("Converting (" + item + ") into (" + transformedItem + ")");
        return transformedItem;
    }

    public boolean isFilter() {
        return filter;
    }

    public void setFilter(boolean filter) {
        this.filter = filter;
    }

    public Validator<ItemDTO> getValidator() {
        return validator;
    }

    public void setValidator(Validator<ItemDTO> validator) {
        this.validator = validator;
    }
}
