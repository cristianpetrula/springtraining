package org.superstore.batch;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by cristian.petrula on 6/28/2015.
 */
public class ArchiveTasklet implements Tasklet {
    private String folder;
    private String file;
    private String archiveFolder;

    public String getArchiveFolder() {
        return archiveFolder;
    }

    public void setArchiveFolder(String archiveFolder) {
        this.archiveFolder = archiveFolder;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        //moveFile(folder + file, archiveFolder + getNewFileName(file));
        return RepeatStatus.FINISHED;
    }

    private String getNewFileName(String fileName) {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd_HH-mm-ss");
        String date = sdf.format(now);
        int lastIndex = fileName.lastIndexOf('.');
        return fileName.substring(0, lastIndex) + "_" + date + fileName.substring(lastIndex, fileName.length());
    }

    private void moveFile(String from, String to) {
        try {
            Files.move(Paths.get(from), Paths.get(to));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
