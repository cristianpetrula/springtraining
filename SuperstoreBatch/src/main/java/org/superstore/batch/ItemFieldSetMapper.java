package org.superstore.batch;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;
import org.superstore.dto.ItemDTO;

/**
 * Created by cristian.petrula on 6/27/2015.
 */
public class ItemFieldSetMapper implements FieldSetMapper<ItemDTO> {
    public ItemDTO mapFieldSet(FieldSet fieldSet) throws BindException {
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setCode(fieldSet.readString("code"));
        itemDTO.setCount(fieldSet.readInt("count", 0));
        itemDTO.setName(fieldSet.readString("name"));
        itemDTO.setImage(fieldSet.readString("image"));
        itemDTO.setPrice(fieldSet.readDouble("price"));
        itemDTO.setDescription(fieldSet.readString("description"));
        return itemDTO;
    }
}
