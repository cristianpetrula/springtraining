package org.superstore.batch;

import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.superstore.dto.ItemDTO;

/**
 * Created by cristian.petrula on 6/28/2015.
 */
public class SuperStoreItemValidator implements Validator<ItemDTO> {

    public void validate(ItemDTO value) throws ValidationException {
        if (value.getCode() == null || value.getCode().isEmpty()) {
            throw new ValidationException("Code is mandatory !");
        }
    }
}
