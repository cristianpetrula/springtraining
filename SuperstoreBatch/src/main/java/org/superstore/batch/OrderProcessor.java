package org.superstore.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.springframework.stereotype.Component;
import org.superstore.domain.ShipmentStatus;
import org.superstore.domain.StoreOrder;

import java.util.Date;

/**
 * Created by cristian.petrula on 7/3/2015.
 */
public class OrderProcessor implements ItemProcessor<StoreOrder, ShipmentStatus> {
    private static final Logger log = LoggerFactory.getLogger(OrderProcessor.class);
    private Validator<StoreOrder> validator;
    private boolean filter = true;

    public ShipmentStatus process(StoreOrder item) throws Exception {
        try {
            validator.validate(item);
        } catch (ValidationException e) {
            if (filter) {
                return null; // filter the order
            } else {
                throw e; // skip the order
            }
        }
        ShipmentStatus shipmentStatus = new ShipmentStatus();
        shipmentStatus.setStoreOrderId(item.getId());
        shipmentStatus.setDate(new Date());
        shipmentStatus.setComment("Sent to shipment");
        shipmentStatus.setStatus("SENT");
        return shipmentStatus;
    }

    public Validator<StoreOrder> getValidator() {
        return validator;
    }

    public void setValidator(Validator<StoreOrder> validator) {
        this.validator = validator;
    }

    public boolean isFilter() {
        return filter;
    }

    public void setFilter(boolean filter) {
        this.filter = filter;
    }
}
