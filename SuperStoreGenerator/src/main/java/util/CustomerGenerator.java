package util;

import org.superstore.domain.Customer;
import org.superstore.domain.StoreOrder;
import util.params.CustomerGeneratorParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristian.petrula on 7/2/2015.
 */
public class CustomerGenerator {
    public List<Customer> generateCustomers(CustomerGeneratorParams param) {
        List<Customer> result = new ArrayList<Customer>();
        for (int i = 0; i < param.getCount(); i++) {
            Customer customer = new Customer();
            customer.setName(param.getName() + " " + i);
            customer.setEmail(param.getEmail() + i);
            customer.setStoreOrderList( new ArrayList<StoreOrder>());
            result.add(customer);
        }
        return result;
    }
}
