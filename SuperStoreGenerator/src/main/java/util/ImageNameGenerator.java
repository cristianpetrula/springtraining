package util;

/**
 * Created by cristian.petrula on 6/27/2015.
 */
public class ImageNameGenerator {
    private static final String[] nameList = {"bolt.jpg", "bag.jpg", "box.jpg", "brand.jpg", "camera.jpg", "cube.jpg",
            "fibers.jpg", "ipod.jpg", "noimage.jpg"};

    public static String getRandomImageName() {
        return nameList[SuperStoreRandomUtils.randInt(0, nameList.length - 1)];
    }
}
