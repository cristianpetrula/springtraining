package util;

import org.superstore.domain.Customer;
import org.superstore.domain.Item;
import util.params.CustomerGeneratorParams;
import util.params.ItemGeneratorParams;

import java.util.List;

/**
 * Created by cristian.petrula on 6/27/2015.
 */
public class MainGenerator {
    public static void main(String[] args) {
        testItemGenerator();
    }

    public static void testItemGenerator() {
        ItemGenerator generator = new ItemGenerator();
        ItemGeneratorParams param = new ItemGeneratorParams();
        param.setCode("B");
        param.setName("Name ");
        param.setDescription("The product has some description");
        param.setCount(100000);
        param.setMinStock(0);
        param.setMaxStock(100);
        List<Item> generatedItems = generator.generateItems(param);
        CSVExport.export(generatedItems, "SuperStoreItems_generated_1.csv");
    }

    public static void testCustomerGenerator() {
        CustomerGeneratorParams param = new CustomerGeneratorParams();
        param.setName("Ion");
        param.setEmail("ion@gmail.com");
        param.setCount(10);
        CustomerGenerator customerGenerator = new CustomerGenerator();
        List<Customer> customerList = customerGenerator.generateCustomers(param);
        for (Customer customer : customerList) {
            System.out.println( customer);
        }
    }
}
