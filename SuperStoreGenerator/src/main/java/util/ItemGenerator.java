package util;

import org.superstore.domain.Item;
import util.params.ItemGeneratorParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristian.petrula on 6/20/2015.
 */
public class ItemGenerator {
    public List<Item> generateItems(ItemGeneratorParams param) {
        List<Item> items = new ArrayList<Item>();
        for (int i = 0; i < param.getCount(); i++) {
            Item item = new Item();
            item.setCode(param.getCode() + i);
            item.setName(param.getName() + i);
            item.setDescription(param.getDescription() + i);
            item.setImage(ImageNameGenerator.getRandomImageName());
            item.setStock(SuperStoreRandomUtils.randInt(param.getMinStock(), param.getMaxStock()));
            item.setPrice( SuperStoreRandomUtils.randDouble(1, 100));
            items.add(item);
        }
        return items;
    }
}
