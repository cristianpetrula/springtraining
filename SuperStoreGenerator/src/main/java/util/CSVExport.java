package util;

import org.superstore.domain.Item;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by cristian.petrula on 6/27/2015.
 */
public class CSVExport {
    private static final String CSV_SEPARATOR = ",";

    public static void export(List<Item> items, String outputFile) {
        FileWriter filewriter = null;
        try {
            filewriter = new FileWriter(outputFile);
            for (Item item : items) {
                filewriter.append(generateLine(item));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (filewriter != null) {
                try {
                    filewriter.flush();
                    filewriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String generateLine(Item item) {
        String output = new String();
        output += item.getCode() + CSV_SEPARATOR;
        output += item.getName() + CSV_SEPARATOR;
        output += item.getDescription() + CSV_SEPARATOR;
        output += item.getImage() + CSV_SEPARATOR;
        output += item.getPrice() + CSV_SEPARATOR;
        output += item.getStock() + "\n";
        return output;
    }
}
