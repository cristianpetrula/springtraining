package util.params;

import lombok.Data;

/**
 * Created by cristian.petrula on 6/27/2015.
 */
@Data
public class ItemGeneratorParams {
    private String code;
    private String name;
    private String description;
    private Integer minStock;
    private Integer maxStock;
    private Integer count;
    private Double price;
}
