package util.params;

import lombok.Data;

/**
 * Created by cristian.petrula on 7/2/2015.
 */
@Data
public class CustomerGeneratorParams {
    private int count;
    private String name;
    private String email;
}
