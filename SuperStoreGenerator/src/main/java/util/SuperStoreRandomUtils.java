package util;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by cristian.petrula on 6/20/2015.
 */
public class SuperStoreRandomUtils {
    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public static double randDouble(double min, double max) {
        Random rand = new Random();
        DecimalFormat df2 = new DecimalFormat("###.##");
        double randomValue = min + (max - min) * rand.nextDouble();
        return Double.valueOf(df2.format(randomValue));
    }
}
