package org.superstore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by cristian.petrula on 6/2/2015.
 */
@Controller
@RequestMapping("/")
public class IndexController {
    @RequestMapping("")
    String index() {
        return "index";
    }
}
