package org.superstore.launcher;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ServerLauncher {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring/server-config.xml");
	}
}
