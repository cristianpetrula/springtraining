package org.superstore.launcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.superstore.domain.Item;
import org.superstore.service.ItemService;
import util.GeneratorParams;
import util.ItemGenerator;

import java.util.List;

/**
 * Created by cristian.petrula on 6/6/2015.
 */
public class ClientLauncher {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/client-config.xml");
        ItemService itemService = (ItemService) context.getBean("itemService");
        saveRandomItems(itemService);
        System.out.println(itemService.echo("Test message"));
    }

    private static void saveRandomItems(ItemService itemService) {
        ItemGenerator itemGenerator = new ItemGenerator();
        GeneratorParams param = new GeneratorParams();
        param.setCode("B");
        param.setName("Name ");
        param.setDescription("The product has some description");
        param.setCount(100);
        param.setMinStock(0);
        param.setMaxStock(100);
        List<Item> itemsToInsert = itemGenerator.generateItems(param);
        for (Item item : itemsToInsert) {
            itemService.save(item);
        }

    }

}
