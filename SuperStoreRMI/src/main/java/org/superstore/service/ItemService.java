package org.superstore.service;

import org.superstore.domain.Item;

public interface ItemService {
	public void save(Item item);
	public String echo(String message);
}
