package org.superstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.superstore.domain.Item;
import org.superstore.repository.ItemRepository;

import javax.transaction.Transactional;

@Service("ItemService")
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemsRepository;

    @Override
    @Transactional
    public void save(Item item) {
        itemsRepository.save(item);
     }

    @Override
    public String echo(String message) {
        return message;
    }
}
