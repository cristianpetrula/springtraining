package org.superstore;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.superstore.domain.Item;
import org.superstore.dto.ItemListDTO;
import util.ItemGenerator;
import util.params.ItemGeneratorParams;

import java.util.List;

@SpringBootApplication
public class RestClient {
    public static final String PATH = "http://localhost:8080/api";

    public static void main(String[] args) {
        //getitems();
        megaInsert2();
    }

    private static void getitems() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        headers.add("Content-Type", "application/json");

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<ItemListDTO> response = null;
        try {
            response = restTemplate.exchange(PATH + "/item", HttpMethod.GET, request, ItemListDTO.class);
        } catch (Exception e) {

        } finally {
            if (response != null) {
                System.out.println(response.getStatusCode());
            } else {
                System.err.println("404");
            }
        }

        if (response != null) {
            ItemListDTO list = response.getBody();
            //ItemListDTO list = restTemplate.getForObject(PATH + "/item", ItemListDTO.class);
            for (Item item : list.getList()) {
                System.out.println(item);
            }
        }
        /*
        System.out.println("Insert new Item");
        InsertItemDTO insertItemDTO = new InsertItemDTO();
        insertItemDTO.setCode("4");
        insertItemDTO.setDescription("Project 4");
        insertItemDTO.setCount(4);
        insertItemDTO.setImgName("NoImage.jpg");
        insertItemDTO.setName("Some name");
        insertItemDTO.setPrice(3f);
        restTemplate.postForEntity(PATH + "/item", insertItemDTO, Void.class);
        list = restTemplate.getForObject(PATH + "/item", ItemListDTO.class);
        for (ItemDTO item : list.getList()) {
            System.out.println(item);
        }
        */
    }

    private static void megaInsert() {
        ItemGenerator itemGenerator = new ItemGenerator();
        ItemGeneratorParams param = new ItemGeneratorParams();
        param.setCode("C");
        param.setName("Name ");
        param.setDescription("The product has some description");
        param.setCount(100000);
        param.setMinStock(0);
        param.setMaxStock(100);
        List<Item> items = itemGenerator.generateItems(param);
        RestTemplate restTemplate = new RestTemplate();
        for (Item item : items) {
            restTemplate.postForObject(PATH + "/item", item, Void.class);
        }
    }

    private static void megaInsert2() {
        ItemGenerator itemGenerator = new ItemGenerator();
        ItemGeneratorParams param = new ItemGeneratorParams();
        param.setCode("C");
        param.setName("Name ");
        param.setDescription("The product has some description");
        param.setCount(10);
        param.setMinStock(0);
        param.setMaxStock(100);
        List<Item> items = itemGenerator.generateItems(param);
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        headers.add("Content-Type", "application/json");
        HttpEntity<ItemListDTO> request = new HttpEntity<ItemListDTO>(headers);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.exchange(PATH + "/item/addlist", HttpMethod.POST, request, Void.class);
        //restTemplate.postForObject(PATH + "/item/addlist", items, Void.class);
    }
}
