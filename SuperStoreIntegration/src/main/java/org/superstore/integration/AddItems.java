package org.superstore.integration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.superstore.domain.Item;
import org.superstore.service.ItemsService;

/**
 * Created by cristian.petrula on 7/1/2015.
 */
@Component
public class AddItems {
    private Logger log = Logger.getLogger(AddItems.class);
    @Autowired
    private ItemsService itemsService;

    @ServiceActivator(inputChannel = "addSingleItem.channel", outputChannel = "log.channel")
    public void processAddRequest(Message<Item> message) {
        itemsService.save(message.getPayload());
        log.info("Saved item with id " + message.getPayload().getId());
    }
}
