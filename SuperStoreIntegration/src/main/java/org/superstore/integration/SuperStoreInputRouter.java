package org.superstore.integration;

import org.apache.log4j.Logger;
import org.springframework.integration.annotation.Router;
import org.springframework.messaging.Message;

/**
 * Created by cristian.petrula on 6/30/2015.
 */
public class SuperStoreInputRouter {
    private Logger log = Logger.getLogger(SuperStoreInputRouter.class);

    @Router
    public String route(Message input) {
        System.out.println(input.getHeaders());
        String type = (String) input.getHeaders().get("TYPE");
        String nextChannel = "unknown.channel";
        if (type.equals("Heartbeat")) {
            nextChannel = "heartbeat.channel,log.channel";
        } else if (type.equals("addItems")) {
            nextChannel = "addListItem.channel";
        }
        log.info("Routing to " + nextChannel + " " + input);
        return nextChannel;
    }
}
