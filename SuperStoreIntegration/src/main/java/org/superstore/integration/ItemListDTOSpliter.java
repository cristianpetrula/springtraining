package org.superstore.integration;

import org.apache.log4j.Logger;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;
import org.superstore.domain.Item;
import org.superstore.dto.ItemListDTO;

import java.util.List;

/**
 * Created by cristian.petrula on 7/4/2015.
 */
@MessageEndpoint
public class ItemListDTOSpliter {
    private Logger log = Logger.getLogger(ItemListDTOSpliter.class);

    @Splitter(inputChannel = "addListItem.channel", outputChannel = "addSingleItem.channel")
    public List<Item> split(ItemListDTO listDTO) {
        List<Item> result = listDTO.getList();
        log.info("Split into " + result.size() + " new messages");
        return result;
    }
}
