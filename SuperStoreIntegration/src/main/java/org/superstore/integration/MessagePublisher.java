package org.superstore.integration;

import org.apache.log4j.Logger;
import org.springframework.integration.annotation.Publisher;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.superstore.domain.SuperStoreMessage;

/**
 * Created by cristian.petrula on 6/30/2015.
 */
@Component
public class MessagePublisher {
    private Logger log = Logger.getLogger(MessagePublisher.class);

    @Publisher(channel = "input.channel")
    public void storeInputMessage(@Header(value = "TYPE") String type, @Payload SuperStoreMessage message) {
        log.info(type + " " + message);
    }
}
