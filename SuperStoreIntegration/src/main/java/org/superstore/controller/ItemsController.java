package org.superstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.superstore.page.CustomPageRequest;
import org.superstore.page.ItemsPageWrapper;
import org.superstore.service.ItemsService;

/**
 * Created by cristian.petrula on 6/2/2015.
 */
@Controller
@RequestMapping("/items")
public class ItemsController {
    @Autowired
    private ItemsService itemsService;

    @RequestMapping("")
    public String items(Model model, Pageable pageable) {
        CustomPageRequest customPageRequest = new CustomPageRequest(pageable);
        ItemsPageWrapper wrapper = new ItemsPageWrapper(itemsService.findAll(customPageRequest));
        model.addAttribute("page", wrapper);
        return "items";
    }


}
