package org.superstore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PropertiesController {

    @RequestMapping("/properties")
    String index() {
        return "properties";
    }

    @RequestMapping("/properties/json")
    @ResponseBody
    java.util.Properties properties() {
        return System.getProperties();
    }
}
