package org.superstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.superstore.service.ItemsServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristian.petrula on 6/2/2015.
 */
@Controller
@RequestMapping("/")
public class IndexController {
    @Autowired
    private ItemsServiceImpl itemsService;

    @RequestMapping("")
    public String index() {

        return "index";
    }
}
