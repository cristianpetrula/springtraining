package org.superstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:integration-context.xml")
public class SuperStoreWithIntegration {
    public static void main(String[] args) {
        SpringApplication.run(SuperStoreWithIntegration.class);
    }
}
