package org.superstore.api.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.superstore.api.service.HeartbeatMessage;
import org.superstore.api.service.MessageService;
import org.superstore.integration.AddItems;

/**
 * Created by cristian.petrula on 6/30/2015.
 */
@Controller
@RequestMapping("/api")
public class APIHeartbeatController {
    private Logger log = Logger.getLogger(AddItems.class);
    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "/heartbeat", method = RequestMethod.GET)
    @ResponseBody
    public void heartbeat() {
        log.info( "Heartbeat call received");
        messageService.publishMessage("Heartbeat", new HeartbeatMessage());
    }
}
