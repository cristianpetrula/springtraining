package org.superstore.api.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.superstore.api.service.MessageService;
import org.superstore.dto.ItemListDTO;
import org.superstore.integration.AddItems;

/**
 * Created by cristian.petrula on 6/9/2015.
 */
@Controller
@RequestMapping("/api/item")
public class APIItemsController {
    private Logger log = Logger.getLogger(AddItems.class);
    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    void save(@RequestBody ItemListDTO item) {
        log.info( "Add items call received.");
        messageService.publishMessage("addItems", item);
    }
}
