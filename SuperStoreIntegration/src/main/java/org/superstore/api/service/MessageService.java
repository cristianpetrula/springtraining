package org.superstore.api.service;

import org.superstore.domain.SuperStoreMessage;

/**
 * Created by cristian.petrula on 6/30/2015.
 */
public interface MessageService {
    public void publishMessage(String type, SuperStoreMessage message);
}
