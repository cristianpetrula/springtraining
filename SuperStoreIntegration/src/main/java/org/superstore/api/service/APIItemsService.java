package org.superstore.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.superstore.domain.Item;
import org.superstore.dto.ItemListDTO;

import java.util.List;

/**
 * Created by cristian.petrula on 6/8/2015.
 */
public interface APIItemsService {
    ItemListDTO findAll(Pageable pageable);
    void save(Item item);
}
