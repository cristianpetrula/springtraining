package org.superstore.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.superstore.domain.SuperStoreMessage;
import org.superstore.integration.MessagePublisher;

/**
 * Created by cristian.petrula on 6/30/2015.
 */
@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    MessagePublisher messagePublisher;

    public void publishMessage(String type, SuperStoreMessage message) {
        messagePublisher.storeInputMessage(type, message);
    }
}
