package org.superstore.dto;

import lombok.Data;
import org.superstore.domain.Item;
import org.superstore.domain.SuperStoreMessage;

import java.util.List;

/**
 * Created by cristian.petrula on 6/9/2015.
 */
@Data
public class ItemListDTO implements SuperStoreMessage{
    private List<Item> list;
}
