package org.superstore.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.superstore.domain.Customer;

/**
 * Created by cristian.petrula on 7/2/2015.
 */
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> {
}
