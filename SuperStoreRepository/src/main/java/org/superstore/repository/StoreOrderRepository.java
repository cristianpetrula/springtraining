package org.superstore.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.superstore.domain.StoreOrder;

/**
 * Created by cristian.petrula on 7/3/2015.
 */
@Repository
public interface StoreOrderRepository extends PagingAndSortingRepository<StoreOrder, Integer> {
}
