package org.superstore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.superstore.domain.Item;

/**
 * Created by cristian.petrula on 6/4/2015.
 */
@Repository
public interface ItemRepository extends PagingAndSortingRepository<Item, Integer> {

}
