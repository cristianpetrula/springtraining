package org.superstore.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.superstore.domain.ShipmentStatus;

/**
 * Created by cristian.petrula on 7/3/2015.
 */
@Repository
public interface ShipmentRepository extends PagingAndSortingRepository<ShipmentStatus, Integer> {
    @Query(value = "select ss from ShipmentStatus ss where ss.storeOrderId = :orderId")
    public ShipmentStatus getByOrderId(@Param("orderId") Integer orderId);
}
