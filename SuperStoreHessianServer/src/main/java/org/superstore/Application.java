package org.superstore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.superstore.service.CustomerService;
import org.superstore.service.CustomerServiceImpl;
import org.superstore.service.ItemsService;
import org.superstore.service.ItemsServiceImpl;

@SpringBootApplication
public class Application {
    @Autowired
    private ItemsServiceImpl itemsService;
    @Autowired
    private CustomerServiceImpl customerService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Bean(name = "/ItemsService")
    public HessianServiceExporter itemsService() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(itemsService);
        exporter.setServiceInterface(ItemsService.class);
        return exporter;
    }

    @Bean(name = "/CustomersService")
    public HessianServiceExporter customersService() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(customerService);
        exporter.setServiceInterface(CustomerService.class);
        return exporter;
    }
}
