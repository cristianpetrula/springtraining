package org.superstore.service;

import org.superstore.domain.Item;

import java.util.List;

/**
 * Created by cristian.petrula on 6/8/2015.
 */
public interface ItemsService {
    public List<Item> findAll();
}
