package org.superstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.superstore.domain.Item;
import org.superstore.repository.ItemRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristian.petrula on 6/4/2015.
 */
@Service
public class ItemsServiceImpl implements ItemsService {
    @Autowired
    private ItemRepository itemsRepository;

    public List<Item> findAll() {
        List<Item> dbItems = (List<Item>) itemsRepository.findAll();
        return dbItems;
    }
}
